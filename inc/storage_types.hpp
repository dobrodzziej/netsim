//
// Created by zelekluk on 08.01.2020.
//

#ifndef NETSIM_STORAGE_TYPES_HPP
#define NETSIM_STORAGE_TYPES_HPP

#include <string>
#include <list>
#include <set>
#include "package.hpp"

enum class PackageQueueType{
    FIFO, LIFO
};

class IPackageStockpile {
public:
    virtual void push(Package&& package)=0;
    virtual bool empty() const = 0;
    virtual size_type size() const = 0;
    using const_iterator = std::list<Package>::const_iterator;
    using stockpile = std::list<Package>;
    virtual const_iterator begin() const = 0;
    virtual const_iterator cbegin() const = 0;
    virtual const_iterator end() const = 0;
    virtual const_iterator cend() const = 0;
};


class IPackageQueue : public IPackageStockpile{
public:
    virtual Package pop() = 0;
    virtual PackageQueueType get_queue_type() const = 0;
};

class PackageQueue : public IPackageQueue{
public:
    explicit PackageQueue(const PackageQueueType &p_q_t): queue_type_(p_q_t){}
    Package pop() override;
    PackageQueueType get_queue_type() const override { return queue_type_;}
    void push(Package&& package) override;
    bool empty() const override { return package_stockpile_.empty();}
    size_type size() const override {return package_stockpile_.size();}
    const_iterator begin() const override {return package_stockpile_.begin();}
    const_iterator cbegin() const override {return package_stockpile_.cbegin();}
    const_iterator end() const override {return package_stockpile_.end();}
    const_iterator cend() const override {return package_stockpile_.cend();}

private:
    PackageQueueType const queue_type_; //TODO usunac const jesli wyjdzie jakis problem

    stockpile package_stockpile_;
};

#endif //NETSIM_STORAGE_TYPES_HPP
