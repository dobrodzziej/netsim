//
// Created by zelekluk on 08.01.2020.
//

#ifndef NETSIM_HELPERS_HPP
#define NETSIM_HELPERS_HPP

#include <functional>
#include <random>

#include "types.hpp"

extern std::random_device rd;
extern std::mt19937 rng;

extern double default_probability_generator();

extern double custom_probability_generator_1();
extern double custom_probability_generator_2();
extern double custom_probability_generator_3();


extern ProbabilityGenerator probability_generator;

#endif //NETSIM_HELPERS_HPP
