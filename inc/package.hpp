//
// Created by zelekluk on 08.01.2020.
//

#ifndef NETSIM_PACKAGE_HPP
#define NETSIM_PACKAGE_HPP

#include <cstdint>
#include <set>
#include "types.hpp"

#define  Haram (-1) // Po arabsku Haram oznacza nie wolno, to co zabronione jest


class Package{
public:
    Package();

    explicit Package(ElementID id);
    Package(const Package&) = delete;
    Package(Package&&) noexcept;

    Package& operator =(Package&) = delete;
    Package& operator =(Package&&) noexcept;

    ~Package();

    ElementID get_id()const{return id_;}
private:

    inline static set_id assigned_ids_={};   // Obecnie wykorzystywane
    inline static set_id freed_ids_={};     // Możliwe do wykorzystania

    ElementID id_ = Haram;
};

#endif //NETSIM_PACKAGE_HPP
