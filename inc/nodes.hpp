//
// Created by zelekluk on 08.01.2020.
//

#ifndef NETSIM_NODES_HPP
#define NETSIM_NODES_HPP

#include <optional>
#include <map>
#include <memory>
#include "storage_types.hpp"
#include "package.hpp"
#include "types.hpp"
#include "helpers.hpp"
#include "config.hpp"

enum class ReceiverType{
    WORKER, STOREHOUSE
};

class IPackageReceiver{
public:
    //gettery
    virtual ElementID get_id() const = 0;
    virtual IPackageStockpile::const_iterator begin() const = 0;
    virtual IPackageStockpile::const_iterator cbegin() const = 0;
    virtual IPackageStockpile::const_iterator end() const = 0;
    virtual IPackageStockpile::const_iterator cend() const = 0;
    virtual ReceiverType get_receiver_type() const = 0; //TODO dla factory wymagane
    //settery
    virtual void receive_package(Package &&) = 0;
};

class ReceiverPreferences{
public:
    //aliasy
    using preferences_t = std::map<IPackageReceiver*, double>;
    using const_iterator = preferences_t::const_iterator;
    using iterator = preferences_t::iterator;
    //konstruktory
    ReceiverPreferences(ProbabilityGenerator ran_fun = probability_generator): rand_generator_(ran_fun){}
    //gettery
    IPackageReceiver* choose_receiver();
    const preferences_t& get_preferences() const { return pref_; }
    iterator begin(){return pref_.begin();}
    const_iterator cbegin() const {return pref_.cbegin();}
    iterator end(){return pref_.end();}
    const_iterator cend() const {return pref_.cend();}
    //settery
    void add_receiver(IPackageReceiver* r);
    void remove_receiver(IPackageReceiver* r);
    void set_preferences(const preferences_t& p){ pref_ = p; }
    //destruktor
    ~ReceiverPreferences() = default;
    //pola publiczne
    preferences_t pref_;
private:
    ProbabilityGenerator rand_generator_;
};

class PackageSender{
public:
    //aliasy
    using buffer = std::optional<Package>;
    //konstruktory
    PackageSender() = default;
    PackageSender(const PackageSender&) noexcept ; // TODO Do przemyslenia ...
    PackageSender(PackageSender&&) = default; //TODO dla factory wymagane
    PackageSender& operator=(const PackageSender&) noexcept ;
    PackageSender&& operator=(PackageSender&&) noexcept ;
    //gettery
    const buffer& get_sending_buffer() const { return buffer_package; }
    //settery
    void send_package();
    //pola publiczne
    ReceiverPreferences receiver_preferences_;
    //destruktor
    ~PackageSender() = default;
protected:
    void push_package(Package&&);
private:
    buffer buffer_package = std::nullopt;
};


class Worker :public IPackageReceiver, public PackageSender{
public:
    //aliasy
    using uni_package = std::unique_ptr<IPackageQueue>;
    //konstruktory
    Worker(ElementID id, TimeOffset pd, uni_package ptr_que): id_(id), uni_package_queue_(std::move(ptr_que)), pd_(pd){}
    Worker(const Worker&) noexcept ; // TODO Do przemyslenia ...
    Worker(Worker&&) = default; //TODO dla factory wymagane
    Worker& operator=(const Worker&) noexcept ;
    Worker&& operator=(Worker&&) noexcept ;
    //gettery
    TimeOffset get_processing_duration(){ return pd_; }
    Time get_package_processing_start_time(){ return start_work_time_; }
    ElementID get_id() const override { return id_;}
    IPackageStockpile::const_iterator cbegin() const override { return uni_package_queue_->cbegin(); }
    IPackageStockpile::const_iterator begin() const override { return uni_package_queue_->begin(); }
    IPackageStockpile::const_iterator end() const override { return uni_package_queue_->end(); }
    IPackageStockpile::const_iterator cend() const override { return uni_package_queue_->cend(); }
    ReceiverType get_receiver_type() const override { return ReceiverType::WORKER;}
    //settery
    void do_work(Time t);
    void receive_package(Package && pack) override {uni_package_queue_->push(std::move(pack));}
    //destruktor
    ~Worker() = default;
private:
    ElementID id_;
    uni_package uni_package_queue_;
    TimeOffset pd_;
    buffer work_buffer_ = std::nullopt;
    Time start_work_time_;
};

class Storehouse : public IPackageReceiver{
public:
    //aliasy
    using uni_stockpile = std::unique_ptr<IPackageStockpile>;
    //konstruktory
    Storehouse(ElementID id, uni_stockpile d = std::make_unique<PackageQueue>(PackageQueueType::FIFO)): id_(id), uni_ptr_stockpointer_(std::move((d))){}
    Storehouse(const Storehouse&) noexcept ; // TODO Do przemyslenia ...
    Storehouse(Storehouse&&) = default; //TODO dla factory wymagane
    Storehouse& operator=(const Storehouse&) noexcept ;
    Storehouse&& operator=(Storehouse&&) noexcept ;
    //gettery
    ElementID get_id() const override { return id_;}
    IPackageStockpile::const_iterator cbegin() const override { return uni_ptr_stockpointer_->cbegin();}
    IPackageStockpile::const_iterator begin() const override { return uni_ptr_stockpointer_->begin();}
    IPackageStockpile::const_iterator end() const override { return uni_ptr_stockpointer_->end();}
    IPackageStockpile::const_iterator cend() const override { return uni_ptr_stockpointer_->cend();}
    ReceiverType get_receiver_type() const override { return ReceiverType::STOREHOUSE;}
    //settery
    void receive_package(Package && pack) override {uni_ptr_stockpointer_->push(std::move(pack));}
    //destruktor
    ~Storehouse() = default;
private:
    ElementID id_;
    uni_stockpile uni_ptr_stockpointer_;
};

class Ramp : public PackageSender{
public:
    //konstruktory
    Ramp(ElementID id, TimeOffset di): di_(di), id_(id) {}
    Ramp(const Ramp&) noexcept ; // TODO Do przemyslenia ...
    Ramp(Ramp&&) = default; //TODO dla factory wymagane
    Ramp& operator=(const Ramp&) noexcept ;
    Ramp&& operator=(Ramp&&) noexcept ;
    //gettery
    TimeOffset get_delivery_interval() const {return di_;}
    ElementID get_id() const {return id_;}
    //settery
    void deliver_goods(Time t);
    //destruktor
    ~Ramp() = default;
private:
    bool start_time_checked_ = false;
    Time ramp_start_time_;
    TimeOffset di_;
    ElementID id_;
};

#endif //NETSIM_NODES_HPP
