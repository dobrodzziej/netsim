//
// Created by zelekluk on 08.01.2020.
//

#ifndef NETSIM_TYPES_HPP
#define NETSIM_TYPES_HPP
#include <functional>
#include <set>

using ElementID = int;
using size_type = uint32_t;
using set_id = std::set<ElementID>;
using Time = uint32_t;
using TimeOffset = uint32_t;   // Czy uint32 jest ok? // Jak najbardziej, jeszcze jak ;-)
using ProbabilityGenerator = std::function<double()>;

#endif //NETSIM_TYPES_HPP
