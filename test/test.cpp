// Pablo korzysta Gcc oraz mingw
// Mamy wyrzucić wszystkie dodatkowe funkcje, metody które żeśmy sobie dopisali, one nie mogą być
// Następnie const powinno być w iteratorach wszystkich, dodatkowo wszystkie funkcje które nic nie modyfikują należy z constem zapisać
// Sprawdzić czy w pętlach for mamy "auto &" a nie samo auto
// Heder guardy jeszcze posprawdzać
// W pierwszym wywołaniu funkcji ramp musi być przekazany do buffora produkt
// Poprawa informacji - mogą być tworzone dodatkowe zmienne w klasach w polach prywatnych


#ifndef TEST_CPP
#define TEST_CPP

#include "gtest/gtest.h"

#include "storage_types.hpp"
#include "package.hpp"
#include "nodes.hpp"
#include "helpers.hpp"
//#include "factory.hpp" TODO w factory przyda się ;-)


//Jaką funkcjonalność testować?
//class PackageSender
//
//        wysyłanie półproduktu: czy po wysłaniu bufor jest pusty?
TEST(PackageSenderTest, testIfBufferEmptyAfterSend)
{
    Ramp source(0,1);
    source.deliver_goods(0);
    Worker lazy_worker(1,1,std::make_unique<PackageQueue>(PackageQueueType::FIFO));
    Worker good_worker(1,1,std::make_unique<PackageQueue>(PackageQueueType::FIFO));
    source.receiver_preferences_ = ReceiverPreferences(probability_generator);
    source.receiver_preferences_.add_receiver(&lazy_worker);
    source.receiver_preferences_.add_receiver(&good_worker);
    source.send_package();
    ASSERT_FALSE(source.get_sending_buffer().has_value());
}
//class ReceiverPreferences
//
//        dodawanie/usuwanie odbiorcy: skalowanie prawdopodobieństwa
TEST(ReceiverPreferencesTest, testAddRemoveReceiver)
{
    Ramp source(0,1);
    source.deliver_goods(0);
    Worker lazy_worker(1,1,std::make_unique<PackageQueue>(PackageQueueType::FIFO));
    Worker good_worker(1,1,std::make_unique<PackageQueue>(PackageQueueType::FIFO));
    source.receiver_preferences_ = ReceiverPreferences(probability_generator);
    source.receiver_preferences_.add_receiver(&lazy_worker);
    double sum_1 = 0;
    double sum_2 = 0;
    for (auto it = source.receiver_preferences_.cbegin() ; it!=source.receiver_preferences_.cend(); ++it)
        sum_1 += it->second;
    source.receiver_preferences_.add_receiver(&good_worker);
    for (auto it = source.receiver_preferences_.cbegin() ; it!=source.receiver_preferences_.cend(); ++it)
        sum_2 += it->second;
    ASSERT_EQ(sum_1, 1.);
    ASSERT_EQ(sum_2, 1.);
}


TEST(ReceiverPreferencesTestRemove, testIfBufferEmptyAfterSend)
{
    Ramp source(0,1);
    source.deliver_goods(0);
    Worker lazy_worker(1,1,std::make_unique<PackageQueue>(PackageQueueType::FIFO));
    Worker good_worker(1,1,std::make_unique<PackageQueue>(PackageQueueType::FIFO));
    source.receiver_preferences_ = ReceiverPreferences(probability_generator);
    source.receiver_preferences_.add_receiver(&lazy_worker);
    source.receiver_preferences_.add_receiver(&good_worker);
    double sum_1 = 0;
    double sum_2 = 0;
    for (auto it = source.receiver_preferences_.cbegin() ; it!=source.receiver_preferences_.cend(); ++it)
        sum_1 += it->second;
    source.receiver_preferences_.remove_receiver(&good_worker);
    for (auto it = source.receiver_preferences_.cbegin() ; it!=source.receiver_preferences_.cend(); ++it)
        sum_2 += it->second;
    ASSERT_EQ(sum_1, 1.);
    ASSERT_EQ(sum_2, 1.);
}

//wybór odbiorcy: czy odpowiedni wybrany, gdy damy “zmockowany” generator liczb losowych?
TEST(ReceiverPreferencesTestRemove, testIfGoodReceiverSelected)
{
    Ramp source(0,1);
    source.deliver_goods(0);
    Worker lazy_worker1(1,1,std::make_unique<PackageQueue>(PackageQueueType::FIFO));
    Worker lazy_worker2(2,1,std::make_unique<PackageQueue>(PackageQueueType::FIFO));
    Worker lazy_worker3(2,1,std::make_unique<PackageQueue>(PackageQueueType::FIFO));
    
    source.receiver_preferences_ = ReceiverPreferences(custom_probability_generator_2);
    source.receiver_preferences_.add_receiver(&lazy_worker1);
    source.receiver_preferences_.add_receiver(&lazy_worker2);
    source.receiver_preferences_.add_receiver(&lazy_worker3);


    auto sel_recv1 = source.receiver_preferences_.choose_receiver();
    auto sel_recv2 = source.receiver_preferences_.choose_receiver();
    EXPECT_EQ(sel_recv1, sel_recv2);

    source.receiver_preferences_ = ReceiverPreferences(custom_probability_generator_3);
    auto sel_recv3 = source.receiver_preferences_.choose_receiver();
    auto sel_recv4 = source.receiver_preferences_.choose_receiver();
    EXPECT_EQ(sel_recv3, sel_recv4);
    EXPECT_FALSE(sel_recv1 == sel_recv3);
}


//class Ramp
//
//        dostawa: czy dostawa odbywa się we właściwej turze? czy półprodukt trafia od razu do bufora?
TEST(RampTest, testIfDeliveryProperlyDone)
{
    Ramp source(0,5);
    Worker lazy_worker(1,1,std::make_unique<PackageQueue>(PackageQueueType::FIFO));
    Worker good_worker(2,1,std::make_unique<PackageQueue>(PackageQueueType::FIFO));
    source.receiver_preferences_ = ReceiverPreferences(probability_generator);
    source.receiver_preferences_.add_receiver(&lazy_worker);
    source.receiver_preferences_.add_receiver(&good_worker);
    Time t = 5;
    while(t--) {
        source.deliver_goods(5-t);
        source.send_package();
        EXPECT_FALSE(source.get_sending_buffer().has_value());
    }

    source.deliver_goods(6);
    EXPECT_EQ(source.get_sending_buffer().has_value(),true);
    source.send_package();
    EXPECT_EQ(source.get_sending_buffer().has_value(),false);

}
//class Worker
//
//        odbiór półproduktu: czy poprawnie wstawiony do kolejki?
TEST(WorkerTestReceive, test) {

    Ramp source(0, 1);
    Worker lazy_worker(1, 5, std::make_unique<PackageQueue>(PackageQueueType::FIFO));
    source.receiver_preferences_ = ReceiverPreferences(probability_generator);
    source.receiver_preferences_.add_receiver(&lazy_worker);
    source.deliver_goods(1);
    ElementID last_elem1 = source.get_sending_buffer()->get_id();
    source.send_package();
    source.deliver_goods(2);
    ElementID last_elem2 = source.get_sending_buffer()->get_id();
    source.send_package();
    source.deliver_goods(3);
    ElementID last_elem3 = source.get_sending_buffer()->get_id();
    source.send_package();
    EXPECT_EQ(last_elem3,(--lazy_worker.cend())->get_id());
    EXPECT_EQ(last_elem2, (++lazy_worker.cbegin()) -> get_id());
    EXPECT_EQ(last_elem1, (lazy_worker.cbegin()) -> get_id());
}
//wykonywanie pracy: czy robotnik przetwarza półprodukt odpowiednią liczbę tur? czy przekazuje dalej odpowiedni półprodukt?
TEST(WorkerTestDoWork, test) {
    Ramp source(0, 1);
    Worker lazy_worker(1, 1, std::make_unique<PackageQueue>(PackageQueueType::FIFO));
    source.receiver_preferences_ = ReceiverPreferences(probability_generator);
    source.receiver_preferences_.add_receiver(&lazy_worker);
    lazy_worker.receiver_preferences_.add_receiver(&lazy_worker);


    source.deliver_goods(1);
    source.send_package();
    EXPECT_EQ(lazy_worker.get_sending_buffer().has_value(), false);
    lazy_worker.do_work(1);
    EXPECT_EQ(lazy_worker.get_sending_buffer().has_value(), false);
    source.deliver_goods(2);
    source.send_package();
    lazy_worker.do_work(2);
    EXPECT_EQ(lazy_worker.get_sending_buffer().has_value(), true);
    lazy_worker.send_package();
    EXPECT_EQ(lazy_worker.get_sending_buffer().has_value(), false);
    source.deliver_goods(3);
    source.send_package();
    lazy_worker.do_work(3);
    EXPECT_EQ(lazy_worker.get_sending_buffer().has_value(), false);
    source.deliver_goods(4);
    source.send_package();
    lazy_worker.do_work(4);
    EXPECT_EQ(lazy_worker.get_sending_buffer().has_value(), true);
}
//class Storehouse
//
//        odbiór półproduktu: czy poprawnie wstawiony do magazynu?

TEST(StorehouseTest, test) {
    Ramp source(0, 1);
    Worker lazy_worker(1, 1, std::make_unique<PackageQueue>(PackageQueueType::FIFO));
    Storehouse destination(2, std::make_unique<PackageQueue>(PackageQueueType::FIFO));
    source.receiver_preferences_ = ReceiverPreferences(probability_generator);
    source.receiver_preferences_.add_receiver(&lazy_worker);
    lazy_worker.receiver_preferences_ = ReceiverPreferences(probability_generator);
    lazy_worker.receiver_preferences_.add_receiver(&destination);
    source.deliver_goods(1);
    ElementID last_elem = source.get_sending_buffer()->get_id();
    source.send_package();
    lazy_worker.do_work(1);
    lazy_worker.do_work(2);
    lazy_worker.send_package();
    EXPECT_EQ(destination.begin()->get_id(), last_elem);
}

#endif