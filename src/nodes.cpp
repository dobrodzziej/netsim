//
// Created by zelekluk on 08.01.2020.
//

#include "nodes.hpp"

void ReceiverPreferences::add_receiver(IPackageReceiver* r)
{
    int number_of_elem = static_cast<int>(pref_.size());
    ++number_of_elem;
    double probability = 1./number_of_elem;
    pref_.emplace(std::make_pair(r,0.));
    for(auto& key: pref_){
        pref_[key.first] = probability;
    }
}

void ReceiverPreferences::remove_receiver(IPackageReceiver* r){
    auto it = std::find_if(pref_.begin(),pref_.end(),[r](const std::pair<IPackageReceiver*,double>& x){return r->get_id() == x.first->get_id();});
    if(it != pref_.end())
        pref_.erase(it);

    int number_of_elem = static_cast<int>(pref_.size());
    double probability;
    if(number_of_elem != 0) {
        probability = 1. / number_of_elem;
        for(auto& key: pref_){
            pref_[key.first] = probability;
        }
    }
}

IPackageReceiver* ReceiverPreferences::choose_receiver(){
    double random_number = rand_generator_();
    int number_of_elem = static_cast<int>(pref_.size());
    double range = 1./number_of_elem;
    double range_begin = 0.;
    double range_last = range;
    for(auto& key: pref_){
        if(random_number == 0. || (range_begin<random_number && random_number<=range_last)){
            return key.first;
        }
        range_begin += range;
        range_last += range;
    }
    return nullptr;
}

void PackageSender::send_package() {
    if(buffer_package.has_value()){
        auto element = receiver_preferences_.choose_receiver();
        element->receive_package(*std::move(buffer_package));
        buffer_package.reset();
    }
}

void PackageSender::push_package(Package&& package) {
    buffer_package.emplace(std::move(package));
}

void Ramp::deliver_goods(Time t) {
    if(!start_time_checked_) {
        start_time_checked_ = true;
        ramp_start_time_ = t;
    }
    if((t-ramp_start_time_)%di_ == 0){
        Package package_elem;
        push_package(std::move(package_elem));
    }
}

void Worker::do_work(Time t) {
    if(work_buffer_.has_value()){
        if((t-start_work_time_+1)%pd_ == 0) {
            push_package(*std::move(work_buffer_));
            work_buffer_.reset();
        }
    }
    else{
        if(!uni_package_queue_->empty()) {
            start_work_time_ = t;
            work_buffer_.emplace(uni_package_queue_->pop());
        }
    }
}