//
// Created by zelekluk on 08.01.2020.
//

#include <package.hpp>
#include <stdexcept>
#include <storage_types.hpp>


void insert_if_not_exists(std::set<ElementID>& s, ElementID e) {
    if (s.find(e) == s.end()) {
        s.insert(e);
    }
}

void erase_if_exists(std::set<ElementID>& s, ElementID e) {
    if (s.find(e) != s.end()) {
        s.erase(e);
    }
}

Package::Package() {
    if (freed_ids_.empty()){
        if(assigned_ids_.empty()) {
            id_ = 1;
        }else{
            id_ = 1 + *assigned_ids_.rbegin();
        }
    }
    else{
        id_ = *freed_ids_.begin();
        freed_ids_.erase(id_);
    }
    assigned_ids_.insert(id_);
}

Package::Package(ElementID id) {
    if (assigned_ids_.find(id) != assigned_ids_.end()) {
        throw std::invalid_argument("The ID of " + std::to_string(id) + " is already assigned!");
    }

    id_ = id;
    assigned_ids_.insert(id_);
    erase_if_exists(freed_ids_, id);
}

Package::Package(Package&& pack) noexcept {
    id_ = pack.id_;
    pack.id_ = Haram;
}

Package& Package::operator =(Package&& pack) noexcept {
    if (id_!=Haram){
        assigned_ids_.erase(id_);
        freed_ids_.insert(id_);
    }
    id_ = pack.id_;
    pack.id_ = Haram;
    return  (*this);
}


Package::~Package(){
    if(id_!=Haram) {
        insert_if_not_exists(freed_ids_,id_);
        erase_if_exists(assigned_ids_,id_);
    }
}
