//
// Created by zelekluk on 08.01.2020.
//

#include "storage_types.hpp"

PackageQueueType IPackageQueue::get_queue_type() const { return PackageQueueType::LIFO; }

Package PackageQueue::pop(){
    Package pack = std::move(*package_stockpile_.begin());
    package_stockpile_.pop_front();
    return pack;
}


void PackageQueue::push(Package&& package){
    switch(queue_type_) {
        case PackageQueueType::FIFO:
            package_stockpile_.emplace_back(std::move(package));
            break;
        case PackageQueueType::LIFO:
            package_stockpile_.emplace_front(std::move(package));
            break;
    }
}
