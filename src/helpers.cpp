//
// Created by zelekluk on 08.01.2020.
//

#include "helpers.hpp"

#include <cstdlib>
#include <random>

// Do generowania wysokiej jakości ciągów liczb pseudolosowych warto użyć
// zaawansowanych generatorów, np. algorytmu Mersenne Twister.
// zob. https://en.cppreference.com/w/cpp/numeric/random
std::random_device rd;
std::mt19937 rng(rd());

double default_probability_generator() {
    // Generuj liczby pseudolosowe z przedziału [0, 1); 10 bitów losowości.
    return std::generate_canonical<double, 10>(rng);
}

double custom_probability_generator_1() {
    return (double) (rand() % RAND_MAX) / RAND_MAX;  // NIEZALECANE: mała losowość generowanego ciągu liczb...
}

double custom_probability_generator_2() {
    return 0.3;  // NIEZALECANE: brak losowości generowanego ciągu liczb...
}
double custom_probability_generator_3() {
    return 0.8;  // NIEZALECANE: brak losowości generowanego ciągu liczb...
}

ProbabilityGenerator probability_generator = default_probability_generator;